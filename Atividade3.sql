﻿-- Trigger - motivação:
-- Não deveríamos ter dados redundantes num banco de dados - como faço para evitar inconsistência nos dados;

drop table if exists conta_corrente cascade;
drop table if exists movimento cascade;
drop table if exists saldo cascade;

create table conta_corrente (
	nu		serial		not null,
	nu_cpf		char(11)	not null,
	nm		varchar(100)	not null,
	constraint pk_conta_corrente
		primary key (nu));

create table movimento (
	nu_seq		serial		not null,
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	ds_historico	varchar(200)	    null,
	constraint pk_movimento
		primary key (nu_seq));

create table saldo (
	nu_conta	integer		not null,
	dt		date		not null,
	vl		numeric(10,2)	not null,
	constraint pk_saldo
		primary key (nu_conta, dt));

alter table movimento
	add constraint fk_movimento_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;

alter table saldo
	add constraint fk_saldo_conta_corrente
		foreign key (nu_conta)
		references conta_corrente;

		
-- Problema: emissão do extrato para um mês/ano específico de um cliente que possui conta corrente à 15 anos;

insert into conta_corrente (nu_cpf, nm) 
values 
('123','Claudio');

insert into movimento (nu_conta, dt, vl, ds_historico)
values 
-- 40.000 movimentos antes de 2020-01-05... registros de 15 anos atrás!!!!
((select nu from conta_corrente where nu_cpf='123'), '2020-01-05',  500, 'Depósito'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-08', -200, 'Pagamento de conta - Luz'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-10', -100, 'Saque'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-03', 2000, 'Salário'),

((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -900, 'Transferência'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -150, 'Pagamento de conta - Telefone'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-08', -400, 'Saque');

-- Saldo inicial = 0
--	nu_conta	Data	Saldo
--		1	05/01	  500
--		1	08/01	  300
--		1	10/01	  200
--		1	03/02	2.200
--		1	06/02	1.150
--		1	08/02	  750

-- Consulta que retorna o último saldo disponível para o correntista de número 1 para a data de referência 05/02/2020.
select	s.vl
from	saldo s
where	s.nu_conta	= 1
and	s.dt		= (   select max(s2.dt)
				from s2.saldo
			       where s2.nu_conta = 1
			       and   s2.dt < '2020-02-05');

-- Extrato do dia 05/02 ao dia 05/03 para o correntista de número de conta 1. 
--	Saldo inicial:  				 2.200
--	06/02	Transferência  				-  900
--	06/02	Pagamento de conta - Telefone		-  150
--	08/02	Saque				 	-  400
-- 	Saldo final:					   750

-- Extrato do dia 11/01 ao dia 03/02. Saldo inicial:   200



-- Cenário 1:
-- O servidor de aplicação para cada movimento inserido/excluído/alterado PELA aplicação deve calcular o saldo e armazená-lo na base;
-- Problemas:
-- Programador novato pode fazer uma implementação errada no cálculo do saldo;
-- Programador novato pode esquecer de executar a rotina de cálculo de saldo para os movimentos atualizados pela aplicação;
-- Alguém pode atualizar a base diretamente, sem passar pela camada de aplicação;
-- Vantagens:
-- A linguagem utilizada pelo servidor de aplicação é de domínio da equipe de desenvolvimento;
-- As ferramentas de debug para as liguagens utilizadas nos servidores de aplicação, são ferramentas bastante robustas, ricas em recursos;


-- Cenário 2:
-- O SGBD, a partir das operações feitas na tabela de movimento (insert, update, delete), atualiza automaticamente o valor do saldo - TRIGGER;
-- Vantagens:
-- Independe de onde se originou a atualização (diretamente na base de dados ou através do servidor de aplicação), o saldo sempre será atualizado;
-- Quem implementa a rotina de atualização tem domínio do SGBD, geralmente não é um profissional novato;
-- Devantagem:
-- A tecnologia (linguagem) geralmente não é a mesma dos servidores de aplicação;
-- As ferramentas de debug dentro do SGBD são inexistens ou muito pobres;
-- O comportamento do trigger é transparente e qualquer falha na implementação não fica tão visível quanto nas aplicações, principalmente pela falta de ferramentas de automação de testes;


-- DESAFIOS:

-- 1. Criar um trigger que, a partir da inserção (INSERT) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

DROP FUNCTION IF EXISTS f_trg_ins_mov();
DROP FUNCTION IF EXISTS f_trg_ins_upd_mov();
DROP FUNCTION IF EXISTS f_trg_ins_upd_del_mov();

CREATE OR REPLACE FUNCTION f_trg_ins_upd_del_mov() 
RETURNS TRIGGER
AS
$$
DECLARE vl_sld_anterior numeric;
BEGIN
	IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
		-- IF (select count(*) from saldo where nu_conta = NEW.nu_conta and dt = NEW.dt) = 0 THEN
		IF NOT EXISTS (select * from saldo where nu_conta = NEW.nu_conta and dt = NEW.dt) THEN
			select	s.vl
			into	vl_sld_anterior		
			from	saldo s
			where	s.nu_conta = NEW.nu_conta
			and	s.dt	   = (   select max(s2.dt)
						   from saldo s2
						  where s2.nu_conta = NEW.nu_conta
						  and   s2.dt < NEW.dt);

			insert into saldo (nu_conta, dt, vl)
			values (NEW.nu_conta, NEW.dt, coalesce(vl_sld_anterior, 0));	
		END IF;

		update	saldo
		set	vl = vl + NEW.vl
		where	nu_conta = NEW.nu_conta
		and	dt >= NEW.dt;
	END IF;

	IF TG_OP = 'UPDATE' OR TG_OP = 'DELETE' THEN
		update	saldo
		set	vl = vl - OLD.vl
		where	nu_conta = OLD.nu_conta
		and	dt >= OLD.dt;
	END IF;
	IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$
LANGUAGE PLPGSQL;

DROP TRIGGER IF EXISTS trg_ins_mov ON movimento;
DROP TRIGGER IF EXISTS trg_ins_upd_mov ON movimento;
DROP TRIGGER IF EXISTS trg_ins_upd_del_mov ON movimento;

CREATE TRIGGER trg_ins_upd_del_mov 
BEFORE INSERT OR UPDATE OF nu_conta, vl, dt OR DELETE 
ON movimento 
FOR EACH ROW
EXECUTE PROCEDURE f_trg_ins_upd_del_mov();


insert into conta_corrente (nu_cpf, nm) 
values 
('123','Claudio');

delete 
from movimento;

delete
from saldo;

insert into movimento (nu_conta, dt, vl, ds_historico)
values 
-- 40.000 movimentos antes de 2020-01-05... registros de 15 anos atrás!!!!
((select nu from conta_corrente where nu_cpf='123'), '2020-01-05',  500, 'Depósito'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-08', -200, 'Pagamento de conta - Luz'),
((select nu from conta_corrente where nu_cpf='123'), '2020-01-10', -100, 'Saque'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-03', 2000, 'Salário'),

((select nu from conta_corrente where nu_cpf='123'), '2020-02-08', -400, 'Saque'),

((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -900, 'Transferência'),
((select nu from conta_corrente where nu_cpf='123'), '2020-02-06', -150, 'Pagamento de conta - Telefone');

select	*
from	movimento;

update	movimento
set	vl = -500
where	nu_conta = (select nu from conta_corrente where nu_cpf='123')
and	dt = '2020-01-10';

delete 
from 	movimento
where	nu_conta = (select nu from conta_corrente where nu_cpf='123')
and	dt = '2020-01-10';

-- ((select nu from conta_corrente where nu_cpf='123'), '2020-01-10', -100, 'Saque'),
-- Update vai funcionar como se fosse um insert (-500) e um delete (-100)
-- "como se fosse sacar 500 e depositar 100"

-- Saldo esperado
-- 05/01	 500
-- 08/01	 300
-- 10/01	 300
-- 03/02	2300
-- 06/02	1250
-- 08/02	 850

select	* 
from 	saldo
order by dt asc;

-- 2. Criar um trigger que, a partir da remoção (DELETE) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

-- 3. Criar um trigger que, a partir da atualização (UPDATE) de dados na tabela MOVIMENTO, atualize adequadamente a tabela SALDO;

-- 4. Criar um mecanismo de AUDITORIA para modificações (INSERT, UPDATE, DELETE) de dados na tabela de MOVIMENTO
-- 4.1. Estrutura de dados para registro da auditoria (nova tabela? histórico? Adicionar campos à tabela movimento?);
-- 4.2. Triggers para alimentar a estrutura de dados;
-- Pergunta que a auditoria tem que responder: "quem" fez "o que" e "quando"?

select CURRENT_USER;
select CURRENT_DATE;
select CURRENT_TIME;
select CURRENT_TIMESTAMP;


DROP FUNCTION IF EXISTS trg_mov_audit()cascade;
DROP FUNCTION IF EXISTS f_mov_audit()cascade;

CREATE OR REPLACE FUNCTION f_mov_audit() RETURNS TRIGGER 
AS 
$$

    BEGIN
      
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO trg_mov_audit SELECT 'DELETE', user, now(), OLD.*;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO trg_mov_audit SELECT 'UPDATE', user, now(), NEW.*;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO trg_mov_audit SELECT 'INSERT', user, now(), NEW.*;
            RETURN NEW;
        END IF;
        RETURN NULL;
    END;

$$
language PLPGSQL;



CREATE TRIGGER trg_mov_audit
AFTER INSERT OR UPDATE OR DELETE ON movimento
    FOR EACH ROW EXECUTE PROCEDURE f_mov_audit();

